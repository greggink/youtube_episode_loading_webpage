#include "stdio.h"
#include <netdb.h>
#include "sys/socket.h"
#include "netinet/in.h"
#include <arpa/inet.h>
#include "string.h"
#include "stdlib.h"

#define BUF_SIZE 4095
#define BUF_SIZE_INC BUF_SIZE + 1
#ifndef NULL
#define NULL '\0'
#endif

int main(int argc, char **argv){

	if(argc < 2){
		printf("Provide domain name\n");
		return 1;
	}

	//DNS
	struct hostent *ht = gethostbyname(argv[1]);
	if(!ht){
		printf("Resolving host failed\n");
		return 1;
	}
	char *ip = inet_ntoa( *( (struct in_addr *) ht->h_addr_list[0]) );

	int sockfd;
	struct sockaddr_in server_addr;
	char header[512];
	int portno = 80;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1){
		printf("error opening socket\n");
		return 1;
	}else{
		printf("socket opened\n");
	}

	memset((char *) &server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(portno);
	inet_pton(AF_INET, ip, &(server_addr.sin_addr) );

	if(connect(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
		printf("failed to connect\n");
		return 0;
	}else{
		printf("connection successful\n");
	}

	snprintf(header, 512, "GET / HTTP/1.1\r\nHost: %s\r\n\r\n", argv[1]);
	printf("Header %s\n", header);

	int nr = send(sockfd, header, strlen(header), 0);
	if(nr){
		printf("nr %d (%lu)\n", nr, strlen(header));
	}

	char *buf;
	buf = (char *) malloc(BUF_SIZE_INC * sizeof(char));
	memset(buf, 0, BUF_SIZE_INC);
	ssize_t bytes_received = recv(sockfd, buf, BUF_SIZE, 0);

	while(bytes_received && bytes_received != 0){
		buf[bytes_received] = 0;
		printf("%s\n", buf);
		memset(buf, 0, BUF_SIZE_INC);
		bytes_received = recv(sockfd, buf, BUF_SIZE, 0);
	}

}//main*/
