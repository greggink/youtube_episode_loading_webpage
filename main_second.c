#include "stdio.h"
#include <netdb.h>
#include <unistd.h>
#include "sys/socket.h"
#include "netinet/in.h"
#include <arpa/inet.h>
#include "string.h"
#include "stdlib.h"

#define TEC_MOST_SIG_BIT 128
#define TEC_2ND_MOST_SIG_BIT 64

#define BUF_SIZE 4095
#define BUF_SIZE_INC BUF_SIZE + 1

char* resolve_host(char *host);
void receive(char *ip, char *domain, char *page);
/*process_header:
	returns size of content
	returns -1 if chunked
*/
char* process_header(char *buf, int *size);

void load_body(int sockfd, char *buf, int size);

/*tec_buf_begins:
	returns 1 if the buffer begins with string str
	returns 0 in all other cases, including errors and str being longer than buffer
*/
int			tec_buf_begins(char *buffer, char *str);

int			tec_string_length(char *str);

/*tec_string_find_char:
	returns index of first instance of a character in a string
	returns -1 if not found
*/
int			tec_string_find_char(char *s, char to_find);

/*tec_string_copy:
	safer alternative to strcpy or even strncpy
	int size is the size of the destination
	these functions guarantee that at most size - 1 bytes will be written to destination plus a NULL character
	this prevents buffer overflows
	this guarantees you get a NULL terminated string in destination (unlike strncpy)
	this function will not cut off the copying in the middle of a UTF8 codepoint when out of space
	returns 1 if all was copied right
	returns 2 if source could not be fully copied
	returns 0 in case of error

	these functions assume char *source is a correctly formatted UTF8 string
*/
int			tec_string_copy(char *destination, char *source, int size);

/*tec_string_find_str:
	look for an instance of to_find in string
	returns index in string where to_find was found
	returns -1 when nothing was found
*/
int			tec_string_find_str(char *string, char *to_find);

/*tec_string_to_int:
	converts a string to an integer
	string may not contain anything in front of number except '-' or '+'
	does not safeguard against overflow
*/
int			tec_string_to_int(char *s);

/*tec_char_is_white_space:
	returns 1 if c is a white space character (e.g. space)
	returns 0 otherwise
	assumes 7 bit ascii character
	there are more white space characters within unicode
	they are not so commonly used and could not all be considered in just an 8 bit character
*/
int			tec_char_is_white_space(char c);

int main(int argc, char **argv){

	if(argc < 2){
		printf("Provide domain name\n");
		return 1;
	}

	// remove "http://"
	char *host = argv[1];
	if( tec_buf_begins(host, "http://") ){
		host += 7;
	}

	// determine domain name & webpage
	char *domain = NULL;
	char webpage[2000] = "/";
	int n = tec_string_find_char(host, '/');
	if(n == -1){
		domain = host;
	}else{
		domain = (char *) malloc(sizeof(char) * n+1);
		tec_string_copy(domain, host, n+1);
		tec_string_copy(webpage, host + n , 2000);
	}

	//resolving host
	char *ip = resolve_host(domain);

	//actually getting webpage
	receive(ip, domain, webpage);

	if(domain != host){
		free(domain);
	}

	return 0;

}//main*/

char* resolve_host(char *domain){

	struct hostent *ht = gethostbyname(domain);
	if(!ht){
		printf("Resolving host failed\n");
		return NULL;
	}

	return inet_ntoa( *( (struct in_addr *) ht->h_addr_list[0]) );

}//resolve_host*/

void receive(char *ip, char *domain, char *page){

	int sockfd, portno;
	struct sockaddr_in server_addr;
	char header[512];
	portno = 80;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd == -1){
		printf("error opening socket\n");
		return;
	}else{
		printf("socket opened\n");
	}

	memset((char *) &server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(portno);
	inet_pton(AF_INET, ip, &(server_addr.sin_addr) );

	int err = 0;
	if( (err = connect(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) ) < 0){
		printf("failed to connect %d\n", err);
		return;
	}else{
		printf("connection successful\n");
	}

	snprintf(header, 512, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", page, domain);
	printf("Header %s\n", header);

	int nr = send(sockfd, header, tec_string_length(header), 0);
	if(nr){
		printf("nr %d (%u)\n", nr, tec_string_length(header));
	}

	char *buf;
	buf = (char *) malloc(BUF_SIZE_INC * sizeof(char));
	memset(buf, 0, BUF_SIZE_INC);
	ssize_t bytes_received = recv(sockfd, buf, BUF_SIZE, 0);

	int size = 0;
	char *new_buffer = process_header(buf, &size);
	memmove(buf, new_buffer, BUF_SIZE_INC - (new_buffer - buf) );
	load_body(sockfd, buf, size);
	puts("*****");

	close(sockfd);

}//receive*/

char* process_header(char *buf, int *size){

	if( tec_buf_begins(buf, "HTTP/1.1 200 OK\r\n") ){
		buf += 17;
	}else{
		printf("unexpected header\n");
	}

	int n = 0;
	int end = 0;

	while( !end ){
		if( tec_buf_begins(buf, "Content-Length:") ){
			buf += 16;
			*size = tec_string_to_int(buf);
			printf("fixed length %d\n", *size);
		}

		if( tec_buf_begins(buf, "\r\n") ){
			printf("end of header!\n********************************************************\n");
			end = 1;
		}

		n = tec_string_find_str(buf, "\r\n");
		buf += n + 2;

	}

	return buf;

}//process_header*/

void load_body(int sockfd, char *buf, int size){

	int bytes_received = -1;
	char *buffer = (char *) malloc(4096);

	if(size <= 0){
		return;
	}
	int remaining = size;
	remaining -= tec_string_length(buf);
	puts(buf);
	while(remaining && bytes_received){
		bytes_received = recv(sockfd, buffer, 4095, 0);
		buffer[bytes_received] = 0;
		puts(buffer);
		memset(buffer, 0, 4096);
		remaining -= bytes_received;
	}

}//load_body*/

int tec_buf_begins(char *buffer, char *str){

	if(!buffer)
		return 0;
	if(!str)
		return 0;

	while(*str && *buffer == *str){
		str += 1;
		buffer += 1;
	}

	if(*str){
		return 0;
	}

	return 1;

}//tec_buf_begins*/

int tec_string_length(char *str){

	if(!str)
		return 0;
	if(!*str)
		return 0;

	int len = 0;

#if __x86_64__

	int64_t *str_i = str;
	int64_t addr = (int64_t) str_i;

	// 64 bit computer
	// ensure str is on 8-byte boundary before using speed-up trick
	while( addr&0x0000000000000007 && *str ){
		len += 1;
		str += 1;
		addr = (int64_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 8 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str_i = (int64_t *) str;
	while( !( ( *str_i - 0x0101010101010101 ) & ~(*str_i) & 0x8080808080808080 ) ){
		len += 8;
		str_i += 1;
	}

	str = (char *) str_i;
	while(*str){
		len += 1;
		str += 1;
	}

#else

	int32_t *str32_i = str;
	int32_t addr32 = (int32_t) str32_i;

	// 32 bit computer
	// ensure str is on 4-byte boundary before using speed-up trick
	while( addr32&0x00000003 && *str ){
		len += 1;
		str += 1;
		addr32 = (int32_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 4 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str32_i = (int32_t *) str;
	while( !( ( *str32_i - 0x01010101 ) & ~(*str32_i) & 0x80808080 ) ){
		len += 4;
		str32_i += 1;
	}

	str = (char *) str32_i;
	while(*str){
		len += 1;
		str += 1;
	}

#endif

	return len;

}//tec_string_length*/

int tec_string_find_char(char *s, char to_find){

	if(!s)
		return -1;

	int i = 0;
	while(s[i] && to_find != s[i]){
		i += 1;
	}

	if(s[i]){
		return i;
	}else{
		return -1;
	}

}//tec_string_find_char*/

int tec_string_copy(char *destination, char *source, int size){

	if(!destination)
		return 0;
	if(!source){
		*destination = 0;
		return 1;
	}
	if(size <= 0)
		return 0;

	size -= 1;

	int i = 0;
	while(*source && i < size){
		destination[i] = *source;
		source += 1;
		i += 1;
	}

	// we don't want to cut off the copying in the middle of a UTF8 codepoint
	// firstly check whether the next byte of source is either not present or the start of a codepoint
	if(*source && (*source & TEC_MOST_SIG_BIT) && !(*source & TEC_2ND_MOST_SIG_BIT) ){
		i -= 1;
		// this while loop goes back while we have the 2nd, 3rd or 4th byte of a UTF8 codepoint
		while( (destination[i] & TEC_MOST_SIG_BIT) && !(destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			i -= 1;
		}
		// this goes back from the head of a UTF8 codepoint
		if( (destination[i] & TEC_MOST_SIG_BIT) && (destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			destination[i] = 0;
		}else{
			// should never happen, this would be invalid UTF8
			destination[i] = 0;
			return 0;
		}
	}

	destination[i] = '\0';

	if(*source){
		return 2;
	}else{
		return 1;
	}

}//tec_string_copy*/

int tec_string_find_str(char *string, char *to_find){

	if(!string || !to_find)
		return -1;

	int len = tec_string_length(string);
	int len2 = tec_string_length(to_find);

	if(!len || !len2)
		return - 1;
	if(len2 > len)
		return -1;

	int index = 0;
	int i = 0;
	int j = 0;

	// I don't need to check the full length of string
	// only as far as there is still enough space for it to contain to_find
	int len_loop = len - len2 + 1;
	while(index < len_loop){
		while(index < len_loop && string[index] != to_find[0]){
			index += 1;
		}
		if(string[index] == to_find[0]){
			i = index;
			j = 0;
			while(i < len && j < len2 && string[i] == to_find[j]){
				i += 1;
				j += 1;
			}
			if(j == len2){
				return index;
			}
		}
		index += 1;
	}

	return -1;

}//tec_string_find_str*/

int tec_string_to_int(char *s){

	if(!s)
		return 0;

	int sign = 1;
	int result = 0;

	while(tec_char_is_white_space(*s)){
		s += 1;
	}
	if(*s == '-'){
		sign = -1;
		s += 1;
	}else{
		if(*s == '+'){
			s += 1;
		}
	}

	while(*s){
		if(*s > '9' || *s < '0'){
			return result * sign;
		}
		result *= 10;
		result += *s - '0';
		s += 1;
	}

	return result * sign;

}//tec_string_to_int*/

int tec_char_is_white_space(char c){

	if(c == 32 || c == 9 || c == 10 || c == 11 || c == 12 || c == 13)
		return 1;
	return 0;

}//tec_char_is_white_space*/
